package org.singleservingsoftware.implementations.selenium;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.Step;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.singleservingsoftware.cache.DriverCache;

public class GenericSeleniumStepImplementation {

    private static Logger logger = Logger.getLogger(GenericSeleniumStepImplementation.class.getName());

    /**
     * Loads a page into the Driver that is cached
     *
     * @param page The URL to load
     */
    @Step("Navigate to <page>")
    public void navigateToPage(String page) {
        if (DriverCache.getDriver() == null)
            Assertions.fail("No driver loaded. Did you forget to tag your test with the 'selenium' tag?");

        try {
            DriverCache.getDriver().get(page);
        }
        catch (Exception e) {
            //Log the message
            Gauge.writeMessage("Failed to load webpage");
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Step("Verify SSL is valid")
    public void isSslValid() {
        Assertions.assertTrue(checkIfSslIsValid());
    }

    @Step("Verify SSL is invalid")
    public void isSslInvalid() {
        Assertions.assertFalse(checkIfSslIsValid());
    }

    protected boolean checkIfSslIsValid() {
        return DriverCache.getDriver().getTitle().equalsIgnoreCase("Privacy error");
    }

    /**
     * Check that the Kingland logo is present on the page
     */
    @Step("Verify that the logo is visible")
    public void logoCheck() {
        if (DriverCache.getDriver() == null)
            Assertions.fail("No driver loaded. Did you forget to tag your test with the 'selenium' tag?");

        Assertions.assertTrue(
                DriverCache.getDriver().findElement(By.cssSelector("img")).isDisplayed(),
                "Logo is not visible on the page!");
    }

    @Step("Fail the test")
    public void implementation1() {
        Assertions.fail();
    }
}
