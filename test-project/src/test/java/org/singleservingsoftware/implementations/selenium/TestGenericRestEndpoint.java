package org.singleservingsoftware.implementations.selenium;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import com.thoughtworks.gauge.Step;

public class TestGenericRestEndpoint {

    @Step("Hit GET endpoint <endpoint>")
    public void getEndpoint(String endpoint) throws InterruptedException, IOException {

        var client = HttpClient.newHttpClient();
        var httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(endpoint))
                .GET()
                .build();

        HttpResponse<String> resp = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        if(resp.statusCode() > 400) {
            throw new IOException(String.format("Failed Request. Return: %d", resp.statusCode()));
        }
    }
}
