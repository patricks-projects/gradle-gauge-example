# Generic API request to ensure the get requests work properly


## Test we can make an example GET request via gauge
Tags: get-request-success

* Hit GET endpoint "https://httpbin.org/get"

## Test we can load a website in saucelabs
Tag: selenium

* Navigate to "https://www.google.com"
* Verify that the logo is visible