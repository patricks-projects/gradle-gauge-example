package org.singleservingsoftware.cache;

import java.util.logging.Logger;

import com.thoughtworks.gauge.ExecutionContext;
import com.thoughtworks.gauge.datastore.ScenarioDataStore;

import org.openqa.selenium.WebDriver;
import org.singleservingsoftware.utils.SeleniumDriverUtils;

/**
 * Acts as a mechanism for passing the WebDriver around between StepImplementations
 */
public class DriverCache {

    private static Logger logger = Logger.getLogger(DriverCache.class.getName());

    public static String getSessionID() {
        return (String) ScenarioDataStore.get("sessionId");
    }

    public static void setSessionID(String sessionID) {
        ScenarioDataStore.put("sessionId", sessionID);
    }

    public static WebDriver getDriver() {

        //If the specStore doesn't have a driver, then initialize one.
        if(ScenarioDataStore.get("driver") == null)
        {
            ExecutionContext context = (ExecutionContext) ScenarioDataStore.get("context");

            if(System.getenv("SAUCE_USERNAME") != null && System.getenv("SAUCE_PASSWORD") != null) {
                DriverCache.setDriver(SeleniumDriverUtils.getSauceLabsWebDriver(context));
            }
            else {
                logger.info("No SauceLabs credentials present. Check SAUCE_USERNAME and SAUCE_PASSWORD if you intend" +
                        "to be running this test on SauceLabs. Defaulting to Local WebDriver");
                DriverCache.setDriver(SeleniumDriverUtils.getLocalWebDriver());
            }
        }

        //At this point, the driver exists, so return it
        return (WebDriver) ScenarioDataStore.get("driver");
    }

    public static void setDriver(WebDriver driver) {
        ScenarioDataStore.put("driver", driver);
    }
}
