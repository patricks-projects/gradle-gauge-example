package org.singleservingsoftware.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.thoughtworks.gauge.ExecutionContext;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.singleservingsoftware.cache.DriverCache;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * Extraction of utilities for creating a WebDriver context, either locally or for running on SauceLabs.
 */
public class SeleniumDriverUtils {

    private static Logger logger = Logger.getLogger(SeleniumDriverUtils.class.getName());

    /**
     * The URL to use for saucelabs. Really shouldn't ever change.
     */
    private static String sauceURL = "https://%s:%s@ondemand.saucelabs.com/wd/hub";

    /**
     * Creates a local WebDriver using a configured browser to determine which driver to run. Driver is configured
     * using the "SELENIUM_BROWSER" environment variable
     *
     * If no driver is
     * @return
     */
    public static WebDriver getLocalWebDriver() {
        String browser = System.getenv("SELENIUM_BROWSER");
        Boolean ignoreSSLForChrome = Boolean.valueOf(System.getenv("IGNORE_SSL_FOR_CHROME"));

        if(browser == null)
        {
            logger.fine("No browser configured, defaulting to Chrome. Change using the SELENIUM_BROWSER env variable.");
            browser = "chrome";
        }

        WebDriver toReturn = null;
        switch(browser) {
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                toReturn = new FirefoxDriver();
                break;

            case "edge":
                WebDriverManager.edgedriver().setup();
                toReturn = new EdgeDriver();
                break; // I mean, it's edge. It probably will.

            default: //chrome
                WebDriverManager.chromedriver().browserInDocker().enableVnc().enableRecording().setup();

                //Checks if env variable is true and gives the argument to ignore cert errors to the driver
                if (ignoreSSLForChrome) {
                    ChromeOptions options = new ChromeOptions();
                    String arg = "--ignore-certificate-errors";
                    options.addArguments((String) arg);
                    toReturn = new ChromeDriver(options);
                } else {
                    toReturn = new ChromeDriver();
                }

        }

        return toReturn;
    }

    /**
     * Creates the SauceLabs web driver to use for running tests
     *
     * Most of this code is pulled from here: https://wiki.saucelabs.com/display/DOCS/Java+Test+Setup+Example
     * @return
     *  A WebDriver pointing to SauceLabs
     */
    public static WebDriver getSauceLabsWebDriver(ExecutionContext context) {
        String username = System.getenv("SAUCE_USERNAME") ;
        String password = System.getenv("SAUCE_PASSWORD");

        Map<String, Object> sauceOptions = new HashMap<>();
        // The context or specification really shouldn't be null, but we've seen this happen so just in case.
        if (context != null && context.getCurrentSpecification() != null && context.getCurrentScenario() != null) {
            
            sauceOptions.put("name",
                "(" + context.getCurrentSpecification().getName() + ")" + context.getCurrentScenario().getName());
            
            //Add our tags to the options so we can sort by them in SauceLabs later if we want
            sauceOptions.put("tags", context.getAllTags());
        }        

        //if "TUNNEL_IDENTIFIER" is present within our environment variables, add that to our capabilities
        String tunnelName = System.getenv("TUNNEL_IDENTIFIER");
        if(tunnelName != null) {
            logger.fine(String.format("Tunnel name has been passed in, attempting to use tunnel %s", tunnelName));
            sauceOptions.put("tunnelIdentifier", tunnelName);
        }

        //check if we're running on a CI build, and use that as the build name if we are. Otherwise set to "local"
        // and use a timestamp
        if(System.getenv("CI_JOB_ID") != null) { //Check for GitLab
            sauceOptions.put("build", "GitLab-" + System.getenv("CI_JOB_ID"));
        }
        else { //Default to local
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            sauceOptions.put("build", "Local-" + format.format(new Date()));
        }

        // No capabilities required, since the w3c was removed.
        ChromeOptions chromeOpts = new ChromeOptions();
        chromeOpts.setPlatformName("Windows 10");
        chromeOpts.setCapability("sauce:options", sauceOptions);

        RemoteWebDriver driver = null;
        try {
            String formattedUrl = String.format(sauceURL, username, password);
            driver = new RemoteWebDriver(new URL(formattedUrl), chromeOpts);
            driver.setFileDetector(new LocalFileDetector());
        } catch (MalformedURLException e) {
            logger.severe("Unable to create webdriver - bad saucelabs URL");
            throw new RuntimeException(e);
        }

        DriverCache.setSessionID(driver.getSessionId().toString());

        return driver;
    }




}
